#compute the key to associate to the VM

module "vault_private-tls-cert" {
  source  = "hashicorp/vault/google//modules/private-tls-cert"
  version = "0.0.4"

#------------------------------------#
#  Edit variables below accordingly  #
#------------------------------------#

ca_public_key_file_path = "/etc/sslkeys/ca.crt.pem"
public_key_file_path = "/etc/sslkeys/jupyterhub.crt"
private_key_file_path = "/etc/sslkeys/jupyterhub.key"
owner = "root"
organization_name = "Percona"
ca_common_name = "percona_ca"
common_name = "percona_cn"
dns_names = [ "giovanniguerrieri.it" ]
#ip_addresses = [ "192.168.0.108", ]
#validity_period_hours = 8760

}



resource "openstack_compute_keypair_v2" "test-keypair" {
  name = var.sshkey
}

#=====================================================================================================

#setup of the VM instance considering the specific parameters of openstack cern
resource "openstack_compute_instance_v2" "test1" { 					
						  							
  name            = var.hostname
  image_name      = var.image
  flavor_name     = var.flavor
  key_pair        = "${openstack_compute_keypair_v2.test-keypair.name}"
  security_groups = ["default"]
  #set the server to allow remote access with custom password (psw config performed during the setup)
  user_data       = "${file("user_data.txt")}"   							


 #do something in the instance remote directory
 provisioner "remote-exec" {       									
    inline = [
#	 "sudo touch /boot/grub/menu.lst",
#	 "sudo update-grub2",
#	 "sudo apt update",
#	 "sudo apt --yes upgrade",
#	 "sudo apt update",
    "echo ==========================",
    "echo Installing Zerotier",
    "echo ==========================",
    "sudo curl -s https://install.zerotier.com | sudo bash",
    "sudo zerotier-cli join ${var.zerotier}",
    "echo ==========================",
    "echo Installing proxy",
    "echo ==========================",
    #install the configurable proxy (option always yes activated)
    "curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -",                                             
    "sudo apt install -y nodejs",
    "sudo npm install -g configurable-http-proxy",
    "echo ==========================",
    "echo Installing JH",
    "echo ==========================",
    #install JH
    "sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py",   	
    "sudo apt-get install python3-distutils",
    "sudo python3 get-pip.py",
    "sudo pip3 install jupyter",
    "sudo pip3 install jupyterhub",
    "jupyterhub --generate-config",
    #"sudo adduser jupyterhubtest",
    "sudo jupyterhub --no-ssl --port 80",					
        ]

  #set the type of connection with the user and the psw previoulsy set
  connection {												
    type     = "ssh"
    user     = var.username
    password = var.password
    host     = "${openstack_compute_instance_v2.test1.access_ip_v4}"
  }  
 }
}
