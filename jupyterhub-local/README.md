# Running the notebook by itself

This is a simple example of running jupyter notebook (integrated with ROOT@CERN)

## Start the notebook

The following comand will:

- pull the correct image
- expose port 8888
- automathically remove the container when closed or stopped.

```bash
docker run -p8888:8888 soap2g/root_notebook:latest
```

Now we should have the notebook running on port 8888 on our docker host.
In this specific case, access via the browser by pasting in a browser the url provided by the terminal. It should be something like
(The token will be different for you)

```bash
 To access the notebook, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-15-open.html
    Or copy and paste one of these URLs:
        http://4c61742ed77c:8888/?token=34b7f124f6783e047e796fea8061c3fca708a062a902c2f9
     or http://127.0.0.1:8888/?token=34b7f124f6783e047e796fea8061c3fca708a062a902c2f9
```



# Running JupyterHub in docker

This is a simple example of running jupyterhub in a docker container.

This example will:

- create a docker network
- run jupyterhub in a container
- enable 'dummy authenticator' for testing
- run users in their own containers

It does not:

- enable persistent storage for users or the hub
- run the proxy in its own container

## Initial setup

The first thing we are going to do is create a network for jupyterhub to use.

```bash
docker network create jupyterhub
```

Second, we are going to build our hub image. First, in a terminal cd into the `Jupyterhub` folder, then execute:

```bash
docker build -t hub .
```

We also want to pull the image that will be used:

```bash
docker pull soap2g/root_notebook
```

## Start the hub

To start the hub, we want to:

- run it on the docker network
- expose port 8000
- mount the host docker socket

```bash
docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock --net jupyterhub --name jupyterhub -p8000:8000 hub
```

Now we should have jupyterhub running on port 8000 [http://localhost:8000/](http://localhost:8000/) on our docker host.

---

## References
* https://jupyterhub-dockerspawner.readthedocs.io/en/latest/
* Docker Hub for development --> https://hub.docker.com/r/soap2g/root_notebook
