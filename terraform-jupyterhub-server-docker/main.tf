#compute the key to associate to the VM

resource "openstack_compute_keypair_v2" "test-keypair" {
  name = var.sshkey
}

#=====================================================================================================

#setup of the VM instance considering the specific parameters of openstack cern
resource "openstack_compute_instance_v2" "test1" { 					
						  							
  name            = var.hostname
  image_name      = var.image
  flavor_name     = var.flavor
  key_pair        = "${openstack_compute_keypair_v2.test-keypair.name}"
  security_groups = ["default"]
  #set the server to allow remote access with custom password (psw config performed during the setup)
  user_data       = "${file("user_data.txt")}"   							

  
#do something in the local terraform directory on ur pc
#provisioner "local-exec" {       									
#   command = <<EOT
#     echo ciao;
#     mkdir soap
#  EOT

 #do something in the instance remote directory
 provisioner "remote-exec" {       									
    inline = [
#	 "sudo touch /boot/grub/menu.lst",
#	 "sudo update-grub2",
#	 "sudo apt update",
#	 "sudo apt --yes upgrade",
#	 "sudo apt update",
    "echo ==========================",
    "echo Installing Zerotier",
    "echo ==========================",
    "sudo curl -s https://install.zerotier.com | sudo bash",
    "sudo zerotier-cli join ${var.zerotier}",
    "echo ==========================",
    "echo Installing proxy",
    "echo ==========================",
    #install docker
    #"sudo apt-get install docker-engine -y",
    "sudo apt install docker.io",
    "sudo service docker start",
    "git clone ssh://git@gitlab.cern.ch:7999/atlas-open-data-iac-qt-2021/orchestrated-docker-notebook-suite.git",			
        ]

  #set the type of connection with the user and the psw previoulsy set
  connection {												
    type     = "ssh"
    user     = var.username
    password = var.password
    host     = "${openstack_compute_instance_v2.test1.access_ip_v4}"
  }  
 }
}
